# Almundo Challenge

[Challenge](https://almundo.com.ar/challenge/home)

[Ranking](https://almundo.com.ar/challenge/ranking)

```console
pip install pipenv --user

git clone git@gitlab.com:redelmann/almundo_challenge.git

cd almundo_challenge
pipenv install --dev

pipenv run jupyter notebook
```