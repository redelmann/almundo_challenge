import json
import pandas as pd

from collections import Counter


TRAIN_FILE = './data/train.json'
TEST_FILE = './data/test.json'


def get_train():
    DROP_COL = ['birthday', 'id', 'gender']

    with open(TRAIN_FILE) as f:
        return pd.DataFrame(json.load(f)).drop(DROP_COL, axis=1)


def _get_test():
    with open(TEST_FILE) as f:
        return json.load(f)


def _get_targets(a):
    """ yield in every unique item combination

    Ex:
    a = [1, 2, 3, 3, 2]

    yield in: [1, 2], 3
              [1, 3], 2
              [2, 3], 1
    """
    u = list(set(a))
    for i in range(len(u)):
        data = u[:i] + u[i+1:]
        target = u[i:i+1][0]
        yield data, target


def _get_visits(visits):
    """ return dict of item counts

    Ex: visits = [1,1,2,3,3]

    Return:
        {
            '1': 2,
            '2', 1,
            '3', 1
        }
    """
    visits_str = [str(x) for x in visits]
    c = dict(Counter(visits_str))
    return c


def data_augmentation(df):
    """ Split df rows into multiple rows using every
    visit as target.
    """
    for _, row in df.iterrows():
        for codes, target in _get_targets(row['codes']):
            new_row = {'country': row['country']}
            visits_row = _get_visits(codes)

            yield {**new_row, **visits_row}, target


def create_test():
    test_X = []

    for row in _get_test():
        country = {'country': row['country']}
        visits = _get_visits(row['codes'])
        test_X.append({**country, **visits})

    return test_X


def get_test():
    return [(x['id'], x['codes']) for x in _get_test()]


